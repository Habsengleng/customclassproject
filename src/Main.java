public class Main {
    public static void main(String[] args) {
        CustomClass<Integer> customClass = new CustomClass<>();
        try{
            customClass.add(1);
            customClass.add(1);
            customClass.add(null);
            customClass.add(4);
//            for(int i=1;i<=1000;i++){
//                customClass.add(i);
//            }
        }catch (NumberFormatException n) {
            System.out.println(n.toString());
        }catch (DuplicateException d) {
            System.out.println(d.toString());
        }
        System.out.println("List all from input:");
        for(int j=0;j<customClass.size();j++){
            System.out.println(customClass.get(j));
        }
    }
}
