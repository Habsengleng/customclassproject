import java.util.Arrays;

public class CustomClass<E> {
    private int default_capacity = 10;
    private  Object[] element;
    private int size=0;
    public CustomClass(int capacity){
        if(capacity>0){
            this.element = new Object[capacity];
        }
        else if(capacity==0){
            this.element= new Object[default_capacity];
        }else {
            throw new IllegalArgumentException("Illegal Capacity: "+
                    capacity);
        }
    }
    public CustomClass(){
        this.element=new Object[default_capacity];
    }

    public int size(){
        return size;
    }
    private boolean contain(E e){
        boolean b=false;
        for(Object ob : element){
            if(ob==e){
                b=true;
            }
        }
        return b;
    }
    public void add(E e) {
        if(e==null){
            throw new NumberFormatException("Inputted null value");
        }
        else if(contain(e)){
            throw new DuplicateException("Duplicate value: " + e);
        }
        else{
            if(default_capacity==size){
                default_capacity=default_capacity+(default_capacity/2);
                element = Arrays.copyOf(element,default_capacity);
            }
            element[size] = e;
            size++;
        }
    }
    public E get(int index){
        return elementData(index);
    }

    E elementData (int index){
        return (E) element[index];
    }
    public int capacity(){
        return element.length;
    }

}
